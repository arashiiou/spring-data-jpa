package idv.kevin.spring.repository;

import idv.kevin.spring.entity.Locate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface LocateRepository extends JpaRepository<Locate, Integer>, JpaSpecificationExecutor<Locate> {

    List<Locate> findByProvince(String province);

    List<Locate> findByProvinceLike(String province);

    List<Locate> findByProvinceAndCity(String province, String city);

    List<Locate> findByIdLessThan(Integer id);

    List<Locate> findByIdGreaterThanEqual(Integer id);

    List<Locate> findByIdBetween(Integer startId, Integer endId);

    List<Locate> findByIdIn(List<Integer> ids);
}
