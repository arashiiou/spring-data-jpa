package idv.kevin.spring.repository;

import idv.kevin.spring.entity.ArticleData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ArticleDataRepository extends JpaRepository<ArticleData, Integer>, JpaSpecificationExecutor<ArticleData> {
}
