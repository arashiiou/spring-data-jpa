package idv.kevin.spring.repository.Impl;

import idv.kevin.spring.dao.PersonDAO;
import idv.kevin.spring.entity.Person;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class PersonRepositoryImpl implements PersonDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void test() {
        Person person = entityManager.find(Person.class, 11);
        System.out.println(person);
    }
}