package idv.kevin.spring.repository;

import idv.kevin.spring.entity.ArticleComment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ArticleCommentRepository extends JpaRepository<ArticleComment, Integer>, JpaSpecificationExecutor<ArticleComment> {
}
