package idv.kevin.spring.repository;

import idv.kevin.spring.dao.PersonDAO;
import idv.kevin.spring.entity.Person;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person, Integer>, JpaSpecificationExecutor<Person>, PersonDAO {

    Person getByLastName(String lastName);

    List<Person> getByLocate_IdGreaterThan(Integer id);

    @Query("select p from Person p where p.lastName=?1 and p.eamil=?2")
    List<Person> testQueryAnnotationParams1(String lastName, String email);

    @Query("select p from Person p where p.lastName= :lastName and p.eamil= :email")
    List<Person> testQueryAnnotationParams2(@Param("email") String email, @Param("lastName") String lastName);

    @Query("select p from Person p where p.lastName like %:lastName%")
    List<Person> testQueryAnnotationParamsLike(@Param("lastName") String lastName);

    @Query("select p from Person p where p.eamil like %?1%")
    List<Person> testQueryAnnotationParamsLike2(String email);

    @Query(value = "select count(id) from JPA_PERSON", nativeQuery = true)
    Integer getCount();

    @Modifying
    @Query("update Person p set  p.eamil = :email where id = :id")
    Integer updatePersonEmail(@Param("email") String mail, @Param("id") Integer id);

    @Query("select p from Person p where p.eamil like %:email%")
    List<Person> testQueryAnnotationParamsLikeWithPageable(Pageable pageable, @Param("email") String email);

    @Query("from Person p where p.lastName =:#{#person.lastName}")
    List<Person> testQueryByObject(@Param("person") Person person);
}
