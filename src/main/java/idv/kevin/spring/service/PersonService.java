package idv.kevin.spring.service;

import idv.kevin.spring.entity.Person;
import idv.kevin.spring.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Transactional
    public void savePersons(List<Person> persons) {
        personRepository.saveAll(persons);
    }

    @Transactional
    public Integer updatePersonEmail(String email, Integer id) {
        return personRepository.updatePersonEmail(email, id);
    }
}
