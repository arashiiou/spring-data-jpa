package idv.kevin.spring.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
//@ToString
@Accessors(chain = true)
@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String author;
    @Column(name = "create_date_time", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createDateTime;
    @Column(name = "upload_date_time", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date uploadDateTime;

    @OneToOne(mappedBy = "article", cascade = CascadeType.PERSIST)
    private ArticleData articleData;

    @OneToMany(mappedBy = "article", cascade = CascadeType.PERSIST)
    private List<ArticleComment> comments = new ArrayList<>();

    @ManyToMany(mappedBy = "articles")
    private List<Type> types = new ArrayList<>();

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", createDateTime=" + createDateTime +
                ", uploadDateTime=" + uploadDateTime +
                ", articleData=" + articleData +
                ", comments=" + comments +
                ", types=" + types +
                '}';
    }
}
