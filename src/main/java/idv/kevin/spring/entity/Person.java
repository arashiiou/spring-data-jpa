package idv.kevin.spring.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString
@Table(name = "JPA_PERSON")
@Entity
public class Person {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Integer id;
    @Column(name = "last_name")
    private String lastName;
    private String eamil;
    private Date birth;

    @Column(name = "l_id")
    private Integer locateId;

    @ManyToOne
    @JoinColumn(name = "locate_id")
    private Locate locate;
}
