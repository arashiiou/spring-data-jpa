package idv.kevin.spring.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "article_comment")
public class ArticleComment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String comment;

    @Column(name = "create_date_time", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date createDateTime;
    @Column(name = "upload_date_time", insertable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date uploadDateTime;

    @ManyToOne
    @JoinColumn(name = "article_Id", referencedColumnName = "id")
    private Article article;

    @Override
    public String toString() {
        return "ArticleComment{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", createDateTime=" + createDateTime +
                ", uploadDateTime=" + uploadDateTime +
                '}';
    }
}
