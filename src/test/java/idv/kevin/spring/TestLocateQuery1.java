package idv.kevin.spring;

import idv.kevin.spring.repository.LocateRepository;
import idv.kevin.spring.entity.Locate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestLocateQuery1 {

    @Autowired
    private LocateRepository locateRepository;

    @Test
    public void testSave() {
        Locate locate = new Locate();
        locate.setProvince("NSW");
        locate.setCity("SYDNEY");

        locateRepository.save(locate);
    }

    @Test
    public void testFindByid() {
        Optional<Locate> optional = locateRepository.findById(1);
        Locate locate = optional.get();
        System.out.println(locate);
    }

    @Test
    public void testFinaAll() {
        List<Locate> locates = locateRepository.findAll();
        locates.forEach(System.out::println);
    }

    @Test
    public void testUpdate() {
        Locate locate = new Locate();
        locate.setProvince("NSW");
        locate.setCity("SYDNEY_");
        locate.setId(2);
        locateRepository.save(locate);
    }

    @Test
    public void testDelete() {
        Locate locate = new Locate();
        locate.setId(2);
        locateRepository.delete(locate);
    }
}
