package idv.kevin.spring;

import idv.kevin.spring.entity.Person;
import idv.kevin.spring.repository.PersonRepository;
import idv.kevin.spring.service.PersonService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SpringDataTest {

    private ApplicationContext ctx = null;
    private PersonRepository personRepository = null;
    private PersonService personService;

    {
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        personRepository = ctx.getBean(PersonRepository.class);
        personService = ctx.getBean(PersonService.class);
    }


    @Test
    public void testCustomRepository() {
        personRepository.test();
    }

    @Test
    public void testQueryByObject() {
        Person person = new Person();
        person.setLastName("hh");
        final List<Person> persons = personRepository.testQueryByObject(person);
        persons.forEach(System.out::println);
    }

    @Test
    public void testJpaSpecificationRepository() {
        int pageNo = 0;
        int pageSize = 10;
        PageRequest pageable = PageRequest.of(pageNo, pageSize);

        Specification<Person> specification = new Specification<Person>() {
            @Override
            public Predicate toPredicate(Root<Person> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Path path = root.get("id");
                Predicate predicate = criteriaBuilder.gt(path, 10);
                return predicate;
            }
        };

        Page<Person> personPage = personRepository.findAll(specification, pageable);

        System.out.println("getTotalPages: " + personPage.getTotalPages());
        System.out.println("getTotalElements: " + personPage.getTotalElements());
        System.out.println("getNumber: " + personPage.getNumber());
        System.out.println("getNumberOfElements: " + personPage.getNumberOfElements());
        System.out.println("getContent: " + personPage.getContent());
    }

    @Test
    public void testJpaRepository2() {
        Person person = new Person();
        person.setLastName("last2");
        person.setEamil("last2@mm.mm");
        person.setBirth(new Date());
        person.setId(27);

        Person person2 = personRepository.saveAndFlush(person);

        System.out.println(person == person2);
    }

    @Test
    public void testJpaRepository() {
        Person person = new Person();
        person.setLastName("last");
        person.setEamil("last@mm.mm");
        person.setBirth(new Date());

        Person person2 = personRepository.saveAndFlush(person);

        System.out.println(person == person2);
    }

    @Test
    public void testPagingAndSortingRepository() {
        int pageNo = 0;
        int pageSize = 10;
        PageRequest pageble = PageRequest.of(pageNo, pageSize, Sort.by("id").descending().and(Sort.by("lastName")));
        Page<Person> personPage = personRepository.findAll(pageble);

        System.out.println("getTotalPages: " + personPage.getTotalPages());
        System.out.println("getTotalElements: " + personPage.getTotalElements());
        System.out.println("getNumber: " + personPage.getNumber());
        System.out.println("getNumberOfElements: " + personPage.getNumberOfElements());
        System.out.println("getContent: " + personPage.getContent());
    }

    @Test
    public void testPagingRepository() {
        int pageNo = 0;
        int pageSize = 10;
        PageRequest pageble = PageRequest.of(pageNo, pageSize);
        Page<Person> personPage = personRepository.findAll(pageble);

        System.out.println("getTotalPages: " + personPage.getTotalPages());
        System.out.println("getTotalElements: " + personPage.getTotalElements());
        System.out.println("getNumber: " + personPage.getNumber());
        System.out.println("getNumberOfElements: " + personPage.getNumberOfElements());
        System.out.println("getContent: " + personPage.getContent());
    }

    @Test
    public void testCRUDRepository() {
        List<Person> persons = new ArrayList<>();

        for (int i = 'a'; i <= 'z'; i++) {
            Person person = new Person();
            person.setBirth(new Date());
            person.setEamil((char) i + "" + (char) i + "@xxx.com");
            person.setLastName((char) (i - 32) + "" + (char) (i - 32));

            persons.add(person);
        }
        personService.savePersons(persons);
    }

    @Test
    public void testUpdate() {
        Integer i = personService.updatePersonEmail("aaaa@xxx.com", 1);
        System.out.println(i);
    }

    @Test
    public void testNativeQuery() {
        Integer count = personRepository.getCount();
        System.out.println(count);
    }

    @Test
    public void testQuery5() {
        Pageable pageable = PageRequest.of(1, 5);
        final List<Person> persons = personRepository.testQueryAnnotationParamsLikeWithPageable(pageable, "xx");
        persons.forEach(System.out::println);
    }

    @Test
    public void testQuery4() {
        List<Person> persons = personRepository.testQueryAnnotationParamsLike2("aa");

        System.out.println(persons);
    }

    @Test
    public void testQuery3() {
        List<Person> persons = personRepository.testQueryAnnotationParamsLike("A");
        System.out.println(persons);
    }

    @Test
    public void testQuery2() {
        List<Person> persons = personRepository.testQueryAnnotationParams2("aa@xxx.com", "AA");

        System.out.println(persons);
    }

    @Test
    public void testQuery() {
        List<Person> persons = personRepository.testQueryAnnotationParams1("AA", "aa@xxx.com");

        System.out.println(persons);
    }

    @Test
    public void testCascade() {
        List<Person> persons = personRepository.getByLocate_IdGreaterThan(1);
        System.out.println(persons);
    }

    @Test
    public void testHelloWorldSpringData() {
        Person person = personRepository.getByLastName("AA");
        System.out.println(person);
    }

    @Test
    public void testJPA() {
    }

    @Test
    public void testDataSource() throws SQLException {
        DataSource ds = ctx.getBean(DataSource.class);
        System.out.println(ds.getConnection());
    }

}
