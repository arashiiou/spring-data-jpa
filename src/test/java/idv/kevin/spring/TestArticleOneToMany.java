package idv.kevin.spring;

import idv.kevin.spring.entity.Article;
import idv.kevin.spring.entity.ArticleComment;
import idv.kevin.spring.repository.ArticleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestArticleOneToMany {

    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void testSave() {
        Article article = new Article();
        article.setTitle("文章一").setAuthor("作者一");

        ArticleComment articleComment1 = new ArticleComment();
        articleComment1.setArticle(article).setComment("評論一~");
        ArticleComment articleComment2 = new ArticleComment();
        articleComment2.setArticle(article).setComment("評論二~");

        ArrayList<ArticleComment> articleComments = new ArrayList<>();
        articleComments.add(articleComment1);
        articleComments.add(articleComment2);

        article.setComments(articleComments);

        Article save = articleRepository.save(article);

        System.out.println(save == article);
    }

    @Test
    @Transactional
    public void testFind() {
        Optional<Article> optional = articleRepository.findById(2);
        Article article = optional.get();
        System.out.println(article);

    }

}
