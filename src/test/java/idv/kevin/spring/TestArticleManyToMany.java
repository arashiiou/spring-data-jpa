package idv.kevin.spring;

import idv.kevin.spring.entity.Article;
import idv.kevin.spring.entity.Type;
import idv.kevin.spring.repository.ArticleRepository;
import idv.kevin.spring.repository.TypeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestArticleManyToMany {

    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private TypeRepository typeRepository;

    @Test
    public void testSave() {
        Article article1 = new Article();
        article1.setTitle("文章一1111").setAuthor("作者一1111");
        Article article2 = new Article();
        article2.setTitle("文章二2222").setAuthor("作者二2222");

        Type type1 = new Type();
        type1.setName("類型一1111");
        Type type2 = new Type();
        type2.setName("類型二2221");


        ArrayList<Type> types = new ArrayList<>();
        types.add(type1);
        types.add(type2);
        article1.setTypes(types);
        article2.setTypes(types);

        ArrayList<Article> articles = new ArrayList<>();
        articles.add(article1);
        articles.add(article2);
        type1.setArticles(articles);
        type2.setArticles(articles);

        articleRepository.save(article1);
        articleRepository.save(article2);

        typeRepository.save(type1);
        typeRepository.save(type2);

    }

    @Test
    @Transactional
    public void testFind() {
        Optional<Article> optional = articleRepository.findById(3);
        System.out.println(optional);
    }

}
