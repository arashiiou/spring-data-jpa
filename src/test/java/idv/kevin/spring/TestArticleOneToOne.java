package idv.kevin.spring;

import idv.kevin.spring.entity.Article;
import idv.kevin.spring.entity.ArticleData;
import idv.kevin.spring.repository.ArticleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestArticleOneToOne {

    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void testSave() {
        Article article = new Article();
        article.setTitle("文章一").setAuthor("作者一");

        ArticleData articleData = new ArticleData();
        articleData.setContent("文章一內容123455654645745364643634");
        articleData.setArticle(article);

        article.setArticleData(articleData);

        Article save = articleRepository.save(article);

        System.out.println(save == article);
    }

    @Test
    public void testFind() {
        Optional<Article> optional = articleRepository.findById(1);
        Article article = optional.get();
        System.out.println(article);

    }

    @Test
    public void testFindByTitle() {
        Article article = articleRepository.findByTitle("文章一");
        System.out.println(article);
    }

}
