package idv.kevin.spring;

import idv.kevin.spring.repository.LocateRepository;
import idv.kevin.spring.entity.Locate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class TestLocateQuery2 {

    @Autowired
    private LocateRepository locateRepository;

    @Test
    public void findAllWithSort() {
        List<Locate> locates = locateRepository.findAll(Sort.by(Sort.Order.desc("id")));
        List<Locate> locates2 = locateRepository.findAll(Sort.by("id").descending());

        for (Locate locate : locates) {
            System.out.println(locate);
        }
        locates2.forEach(System.out::println);
    }

    @Test
    public void findAllWithPage() {

        Page<Locate> page = locateRepository.findAll(PageRequest.of(0, 2, Sort.by("id").descending()));
        page.getContent().forEach(System.out::println);
    }

    @Test
    public void findById() {
        Optional<Locate> optional = locateRepository.findById(1);
        System.out.println(optional.get());
    }

    @Test
    public void findByProvince() {
        final List<Locate> locates = locateRepository.findByProvince("QLD");
        locates.forEach(System.out::println);
    }

    @Test
    public void findByProvinceLike() {
        final List<Locate> locates = locateRepository.findByProvinceLike("%W%");
        locates.forEach(System.out::println);
    }

    @Test
    public void findByProvinceAndCity() {
        final List<Locate> locates = locateRepository.findByProvinceAndCity("QLD", "BRISBANE");
        locates.forEach(System.out::println);
    }

    @Test
    public void findByIdLessThen() {
        final List<Locate> locates = locateRepository.findByIdLessThan(4);
        locates.forEach(System.out::println);
    }

    @Test
    public void findByIdGreaterThanEqual() {
        final List<Locate> locates = locateRepository.findByIdGreaterThanEqual(4);
        locates.forEach(System.out::println);
    }

    @Test
    public void findByIdBetween() {
        final List<Locate> locates = locateRepository.findByIdBetween(2, 5);
        locates.forEach(System.out::println);
    }

    @Test
    public void findByIdIn() {
        Integer[] ids = {4, 2, 3};
        final List<Locate> locates = locateRepository.findByIdIn(Arrays.asList(ids));
        locates.forEach(System.out::println);
    }

    @Test
    public void findAllBySpecification() {
        String province = "QLD";
        String city = "";

        Specification<Locate> specification = new Specification<Locate>() {
            @Override
            public Predicate toPredicate(Root<Locate> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

                List<Predicate> predicaties = new ArrayList<>();
                if (!StringUtils.isEmpty(province)) {
                    predicaties.add(criteriaBuilder.equal(root.get("province").as(String.class), province));
                }
                if (!StringUtils.isEmpty(city)) {
                    predicaties.add(criteriaBuilder.equal(root.get("city").as(String.class), city));
                }
                return criteriaBuilder.and(predicaties.toArray(new Predicate[]{}));
            }
        };

        final List<Locate> locates = locateRepository.findAll(specification);
        locates.forEach(System.out::println);

        final Page<Locate> locatePage = locateRepository.findAll(specification, PageRequest.of(0, 2));
        locatePage.getContent().forEach(System.out::println);

        final Page<Locate> locatePage2 = locateRepository.findAll(specification, PageRequest.of(0, 2, Sort.by("id").descending()));
        locatePage2.getContent().forEach(System.out::println);
    }
}
